<?php

namespace Anegoda1995\OrderDirect;

use GuzzleHttp\Client;
use Anegoda1995\OrderDirect\Exceptions\OrderDirectConnection\JsonError;

class OrderDirectConnection
{
    private $connected = false;
    private $host;
    private $username;
    private $password;
    private $sessionKey;
    
    public function __construct($host, $username, $password, $sessionKey = '')
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->sessionKey = $sessionKey;

        try {
            $this->handle(); //need to get result json for login
        } catch (\Exception $e) {
            $this->connected = false;
            error_log($e->getMessage());
        }
    }

    private function handle()
    {
        if ($this->sessionKey == '') {
            error_log('Session key is empty. Trying to login');
            if (!$this->login()) {
                throw new \Exception('System can\'t login via existed session key, moreover system can\'t login via username and password');
            }
        }

        if (!$this->isExistedSessionKeyValid()) {
            if (!$this->login()) {
                throw new \Exception('System can\'t login via existed session key, moreover system can\'t login via username and password');
            }
        }
        $this->connected = true;
    }

    private function isExistedSessionKeyValid()
    {
        $client = new Client();

        $responseJson = $client->get("http://$this->host/odapi/rest/TMethods/product/$this->sessionKey/product_code_just_for_testing")->getBody()->getContents();
        $responseObj = json_decode($responseJson);
        if (!is_null($responseObj->error)) {
            if (
                $responseObj->error->code === 9022 || // An invalid session key has been specified or the session has expired
                $responseObj->error->code === 9023 || // The session is no longer active
                $responseObj->error->code === 9024)   // Invalid caller id for session
            { 
                return false;
            }
        }
        return true;
    }

    private function login()
    {
        $client = new Client();

        $responseJson = $client->get("http://$this->host/odapi/rest/TMethods/logIn/1/$this->username/$this->password/json")->getBody()->getContents();
        $responseObj = json_decode($responseJson);
        if (!is_null($responseObj->error)) {
            if ($responseObj->error->code === 9006) { // Maximum number of connections exceeded
                return false;
            }
        }
        if (!is_null($responseObj->data)) {
            if (!is_null($responseObj->data->session)) {
                if (!is_null($responseObj->data->session->key)) {
                    $this->sessionKey = $responseObj->data->session->key;
                    $this->connected = true;
                    return true;
                }
            }
        }
        return false;
    }

    public function isConnected()
    {
        return $this->connected;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getSessionKey()
    {
        return $this->sessionKey;
    }

}