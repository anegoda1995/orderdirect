<?php

namespace Anegoda1995\OrderDirect;

use Anegoda1995\OrderDirect\OrderDirectConnection;
use GuzzleHttp\Client;

class OrderDirectCore
{
    private $odConnection;
    
    public function __construct(OrderDirectConnection $odConnection)
    {
        $this->odConnection = $odConnection;
    }

    public function getConnection()
    {
        return $this->odConnection;
    }

    public function getModifiedProducts($fromDateTime, $toDateTime)
    {
        // get products that new in Order Direct system
        $client = new Client();

        $responseJson = $client->get('http://' . $this->odConnection->getHost() . '/odapi/rest/TMethods/productUpdates/' . $this->odConnection->getSessionKey() . "/$fromDateTime/$toDateTime/json/")->getBody()->getContents();
        $responseObj = json_decode($responseJson);
        if (!is_null($responseObj->error)) {
            if (
                $responseObj->error->code === 9022 || // An invalid session key has been specified or the session has expired
                $responseObj->error->code === 9023 || // The session is no longer active
                $responseObj->error->code === 9024)   // Invalid caller id for session
            { 
                return [];
            }
        }
        return $responseObj->data->products;
    }

    public function checkCustomerExistedAndCreateIfNot($customerFirstLastName, $customerEmail)
    {
        $relationId = null;
        // search by customer email (unique)
        $client = new Client();

        $responseJson = $client->get('http://' . $this->odConnection->getHost() . '/odapi/rest/TMethods/relationSearch/' . $this->odConnection->getSessionKey() . "/$customerEmail/json/")->getBody()->getContents();
        $responseObj = json_decode($responseJson);

        if (!is_null($responseObj->error)) {
            if (
                $responseObj->error->code === 9022 || // An invalid session key has been specified or the session has expired
                $responseObj->error->code === 9023 || // The session is no longer active
                $responseObj->error->code === 9024)   // Invalid caller id for session
            { 
                throw new \Exception($responseObj->error->code . ':' . $responseObj->error->message);
            }
        }

        
        // if exist return true
        foreach ($responseObj->data->relations as $relation) {
            $relationId = $relation->relation->number;
            break;
        }

        if ($relationId == null) {
            // if not exist - create and then return true
            $relationId = $this->createCustomer($customerFirstLastName, $customerEmail);
        }
        return $relationId;
        // if some another situation just throw exception
    }

    private function createCustomer($customerFirstLastName, $customerEmail)
    {
        // search by customer email (unique)
        $base_uri = 'http://' . $this->odConnection->getHost() . '/odapi/rest/TMethods/relation/' . $this->odConnection->getSessionKey();
        $client = new Client();

        $body = [
            'data' => [
                'relation' => [
                    'shortName' => $customerFirstLastName,
                    'relationship' => [
                        'types' => [
                            'customer'
                        ]
                    ],
                    'email' => [
                        'address' => $customerEmail
                    ],
                    'active' => true
                ]
            ]
        ];
        $responseJson = $client->put($base_uri, ['json' => $body])->getBody()->getContents();
        $responseObj = json_decode($responseJson);

        if (!is_null($responseObj->error)) {
            if (
                $responseObj->error->code === 9022 || // An invalid session key has been specified or the session has expired
                $responseObj->error->code === 9023 || // The session is no longer active
                $responseObj->error->code === 9024)   // Invalid caller id for session
            { 
                throw new Exception($responseObj->error->code . ':' . $responseObj->error->message);
            }
        }
        return $responseObj->data->relation->number;
    }

    public function getProductPriceForCustomer($relationId, $productCode, $todayDate)
    {
        $client = new Client();

        $responseJson = $client->get('http://' . $this->odConnection->getHost() . '/odapi/rest/TMethods/customerProductPrice/' . $this->odConnection->getSessionKey() . "/$relationId/$productCode/$todayDate/1/json")->getBody()->getContents();
        $responseObj = json_decode($responseJson);

        if (!is_null($responseObj->error)) {
            if (
                $responseObj->error->code === 9022 || // An invalid session key has been specified or the session has expired
                $responseObj->error->code === 9023 || // The session is no longer active
                $responseObj->error->code === 9024)   // Invalid caller id for session
            { 
                throw new Exception($responseObj->error->code . ':' . $responseObj->error->message);
            }
        }
        return $responseObj->data->customerProductPrice->price;
    }

    /**
     * Creating order by Order Drect API and returning result json
     *
     * @param string $relationId
     * @param string $customerEmail
     * @param string $todayDate
     * @param array $products
     * @return void
     */
    public function createOrder($relationId, $customerEmail, $todayDate, $products)
    {
        $lines = [];
        foreach ($products as $key => $product) {
            $lines[] = [
                "line" => [
                    "id" => ++$key,
                    "quantity" => (int) $product['quantity'],
                    "unitPrice" => $product['price'],
                    "product" => [
                        "code" => $product['code'],
                    ]
                ]
            ];
        }

        $base_uri = 'http://' . $this->odConnection->getHost() . '/odapi/rest/TMethods/preliminaryCustomerOrder/' . $this->odConnection->getSessionKey();
        $client = new Client();

        $body = [
            "data" => [
               "preliminaryCustomerOrder" => [
                    "date" => $todayDate,
                    "relation" => [
                        "number" => $relationId,
                        "email" => [
                            "address" => $customerEmail
                        ]
                    ],
                    "lines" => $lines
                ]
            ]
        ];
        $responseJson = $client->put($base_uri, ['json' => $body])->getBody()->getContents();
        $responseObj = json_decode($responseJson);

        if (!is_null($responseObj->error)) {
            if (
                $responseObj->error->code === 9022 || // An invalid session key has been specified or the session has expired
                $responseObj->error->code === 9023 || // The session is no longer active
                $responseObj->error->code === 9024)   // Invalid caller id for session
            { 
                throw new Exception($responseObj->error->code . ':' . $responseObj->error->message);
            }
        }

        return $responseObj;
    }
}