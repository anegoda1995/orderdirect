<?php

namespace Anegoda1995\OrderDirect\Exceptions\OrderDirectConnection;

class JsonError extends \Exception
{
    public function __construct($statusCode, $errorMessage)
    {
        error_log($errorMessage);
    }
}